#!/usr/bin/env python3

import argparse

import numpy as np
from imputation_metrics import metrics
from imputation_metrics.metrics import WassersteinArgs
from imputation_metrics.normalisation import Normalisation


def parse_args() -> tuple[WassersteinArgs, str, str, str]:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--splits",
        "-s",
        help="Number of random data splits",
        type=int,
        default=10,
        dest="n_splits",
    )
    parser.add_argument(
        "--directions",
        "-d",
        help="Number of random directions",
        type=int,
        default=50,
        dest="n_directions",
    )
    parser.add_argument(
        "--marginals",
        "-m",
        help="Use marginals instead of random directions",
        action="store_true",
    )
    parser.add_argument(
        "--normalise-components",
        help="Normalisation to use on data components: do we standardise "
        "each component before doing splits and calculating distances?, "
        "'standardise' or 'none' (default: 'none')",
        choices=["none", "standardise"],
        default="none",
    )
    parser.add_argument(
        "--normalise-distances",
        help="Normalisation to use when calculating Wasserstein distances "
        "between two sets of data, "
        "'standardise' or 'none' (default: 'standardise')",
        choices=["none", "standardise"],
        default="standardise",
    )
    parser.add_argument(
        "--seed", help="Random number generator seed", type=int, default=42
    )
    parser.add_argument(
        "--output",
        "-o",
        help="Filename prefix for output files",
        type=str,
        required=True,
    )
    parser.add_argument(
        "original",
        help="CSV file with original data; is expected to have a header row "
        "but no row index column",
        type=str,
    )
    parser.add_argument(
        "imputed",
        help="CSV file with imputed data; is expected to have a header row "
        "but no row index column",
        type=str,
    )

    args = parser.parse_args()
    w_args = WassersteinArgs(args)

    return w_args, args.original, args.imputed, args.output


def main():
    args, original, imputed, output_prefix = parse_args()
    original_data = np.loadtxt(original, delimiter=",", skiprows=1)
    imputed_data = np.loadtxt(imputed, delimiter=",", skiprows=1)
    metrics.calculate_and_save_wasserstein_distances(
        original_data, imputed_data, output_prefix, args
    )


if __name__ == "__main__":
    main()
