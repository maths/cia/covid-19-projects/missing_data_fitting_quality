#!/usr/bin/env python3

import argparse

from imputation_metrics import experiment_metrics as metrics
from imputation_metrics.experiment_metrics import ImputationArgs
from imputation_metrics.normalisation import Normalisation


def parse_args() -> ImputationArgs:
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--experiment",
        "-e",
        help="Experiment to analyse, 'mimic', 'synthetic', "
        "'syncat' or 'syncat_thresholded'",
        type=str,
        default="mimic",
    )
    parser.add_argument(
        "--splits",
        "-s",
        help="Number of random data splits",
        type=int,
        dest="n_splits",
        default=10,
    )
    parser.add_argument(
        "--directions",
        "-d",
        help="Number of random directions",
        type=int,
        dest="n_directions",
        default=50,
    )
    parser.add_argument(
        "--marginals",
        "-m",
        help="Use marginals instead of random directions",
        action="store_true",
    )
    parser.add_argument(
        "--normalise-components",
        help="Normalisation to use on data components: do we standardise "
        "each component before doing splits and calculating distances?, "
        "'standardise' or 'none' (default: 'none')",
        choices=["none", "standardise"],
        default="none",
    )
    parser.add_argument(
        "--normalise-distances",
        help="Normalisation to use when calculating Wasserstein distances "
        "between two sets of data, "
        "'standardise' or 'none' (default: 'standardise')",
        choices=["none", "standardise"],
        default="standardise",
    )
    parser.add_argument(
        "--seed", help="Random number generator seed", type=int, default=42
    )
    parser.add_argument(
        "--exclude",
        help="Columns to exclude as comma-separated list",
        type=str,
        default="",
    )

    args = parser.parse_args()
    args.experiment = args.experiment.lower()
    if args.exclude:
        args.exclude = list(map(int, args.exclude.split(",")))
    else:
        args.exclude = []
    return ImputationArgs(args)


def main():
    args = parse_args()
    metrics.calculate_and_save_experiment_distances(args)


if __name__ == "__main__":
    main()
