Changelog for Missing Data Fitting Quality package
==================================================

## Version 0.6.3

* Support thresholded syncat (SYNTHETIC_CATEGORICAL_THRESHOLDED)
  dataset in experiment_wasserstein.py script.
* Support excluding columns in distance calculations; this is useful
  for exploring thresholded syncat data.

## Version 0.6.2

* Support syncat (SYNTHETIC_CATEGORICAL) dataset in experiment_wasserstein.py
  script.

## Version 0.6.1

* Allow no seed in WassersteinArgs

## Version 0.6

* Rename `scripts/calculate_wasserstein.py` as
  `scripts/experiment_wasserstein.py` and `imputation_metrics.metrics` as
  `imputation_metrics.experiment_metrics` as these are specific to the
  imputation experiments on the MIMIC and SYNTHETIC datasets.

* `scripts/calculate_wasserstein.py` is now a simpler script that accepts
  two CSV files, one of original data and the other of imputed data, and
  calculates the projected Wasserstein distances between them;
  `imputation_metrics.metrics` is likewise decoupled from the experiment
  setup and takes two datasets as input.

* An example of the use of these appears in the `tests` directory.

* It would be good to rewrite `imputation_metrics.experiments_metrics` to
  use the code from `imputation_metrics.metrics`.

## Version 0.5.1

* First public release
