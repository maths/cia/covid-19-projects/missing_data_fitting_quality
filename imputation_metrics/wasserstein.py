"""Calculate Wasserstein distance between imputed and original data."""

import numpy as np
import ot  # type: ignore

from .normalisation import Normalisation


class WassersteinDistances:
    """Calculate Wasserstein distance of two datasets in various ways.

    Parameters
    ----------
    original_data : np.ndarray
        Original data set, an (n, d) ndarray.
    other_data : np.ndarray
        Other data set, which might be imputed or simulated data, also
        an (n, d) ndarray.
    normalisation : Normalisation
        Method of normalising data.  If NONE, no normalisation will be used.
        If STANDARDISE, then standardise the data by dividing by the
        standard deviation of the original data.  (There is no need to
        subtract the mean, as this does not affect the Wasserstein distance.)

    """

    def __init__(
        self,
        original_data: np.ndarray,
        other_data: np.ndarray,
        normalisation: Normalisation,
    ) -> None:
        self.original_data = original_data
        self.other_data = other_data
        self.normalisation = normalisation

    def feature_dist(self, feature: int) -> float:
        """Calculate the dataset distance for a specific feature.

        This calculates the Wasserstein 2-distance (W_2^2) between the
        specified feature in the two datasets.

        In the case of imputation, this function uses the full set of values
        (including those which were present prior to imputation) for the
        distance calculation.  For a variant which only uses the imputed
        values, see `feature_dist()`.

        Parameters
        ----------
        feature : int
            The column number of the feature to consider: 0, 1, 2, ...,
            `num_fields` - 1.

        Returns
        -------
        distance : float
            The Wasserstein 2-distance.

        """
        original = self.original_data[:, feature]
        other = self.other_data[:, feature]
        original_normalised, other_normalised = self._normalise(
            original, other
        )
        distance = ot.emd2_1d(original_normalised, other_normalised)
        return distance

    def dir_dist(self, direction: np.ndarray) -> float:
        """Calculate the dataset distance in a specified direction.

        This projects the two datasets onto the specified direction (that is,
        a 1-dimensional subspace), and calculates the Wasserstein distance
        between the two resulting distributions.

        Parameters
        ----------
        direction : np.array
            The direction in which to calculate the W_2^2 distance between
            the datasets.

        Returns
        -------
        distance : float
            The calculated W_2^2 distance.

        """
        original = self._project(self.original_data, direction)
        other = self._project(self.other_data, direction)
        original_normed, other_normed = self._normalise(original, other)
        distance = ot.emd2_1d(original_normed, other_normed)
        return distance

    @staticmethod
    def _project(data: np.ndarray, direction: np.ndarray) -> np.ndarray:
        return data @ direction

    def _normalise(
        self, orig: np.ndarray, other: np.ndarray
    ) -> tuple[np.ndarray, np.ndarray]:
        if self.normalisation == Normalisation.NONE:
            return orig, other
        if self.normalisation == Normalisation.STANDARDISE:
            sd = np.std(orig)
            return orig / sd, other / sd
        raise ValueError(
            f"Unrecognised normalisation type: {self.normalisation}"
        )
