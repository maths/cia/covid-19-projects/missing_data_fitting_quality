#!/usr/bin/env python3

"""Calculate measures of distance between imputed data and original data."""

import json
from dataclasses import dataclass
from pathlib import Path
from typing import Union, cast

import numpy as np
import pandas as pd  # type: ignore
from typed_argparse import TypedArgs

from . import config, randoms, read_data
from .normalisation import (Normalisation, normalise_distribution,
                            normalise_distributions)
from .wasserstein import WassersteinDistances


class ImputationArgs(TypedArgs):
    """Arguments for imputation distance calculations.

    experiment : str
        Either "mimic", "synthetic", "syncat" or "syncat_thresholded".
    n_splits : int
        The number of splits of the data.
    n_directions : int
        The number of random directions to calculate distances in per
        simulation.
    marginals : bool
        Use marginals instead of random directions.
    seed : int
        The seed to use for the random direction and split generator.
        The same set of directions and splits will be used for all of the
        parameter combinations (though the splits will be different for
        each holdout set).
    data_normalisation : Normalisation
        Method of normalising given data.  If NONE, no normalisation will be
        used.  If STANDARDISE, then for each column (variable), normalise both
        the original data and imputed data using the mean and standard
        deviation of the original data.
    dist_normalisation : Normalisation
        Method of normalising data when calculating Wasserstein distance
        between two data vectors.  If NONE, no normalisation will be
        used.  If STANDARDISE, then standardise the data by dividing by
        the standard deviation of the original data.  (There is no need to
        subtract the mean, as this does not affect the Wasserstein distance.)
    exclude : list[int]
        Columns to exclude from distance calulations.

    """

    experiment: str
    n_splits: int
    n_directions: int
    marginals: bool
    seed: int
    normalise_components: Normalisation
    normalise_distances: Normalisation
    exclude: list[int]


@dataclass
class ImputationDistances:
    """Hold the results of an imputation distances computation"""

    distances: pd.DataFrame
    baseline_distances: pd.DataFrame
    holdout_splits: dict[int, list[tuple[np.ndarray, np.ndarray]]]
    directions: list[np.ndarray]


def _imputation_distances_for_datasets(
    left_data: np.ndarray,
    right_data: np.ndarray,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
    normalisation: Normalisation,
) -> pd.DataFrame:
    """Calculate imputation distances for specified splits and directions.

    This takes two given sets of data and calculates the imputation distances
    using the specified splits in the specified directions.

    The `left_data` is regarded as the original data set and the `right_data`
    is the data being compared to it.  These two data sets must have the
    same number of rows; the pairs in the list of splits are indices into this
    data.

    Parameters
    ----------
    left_data : np.ndarray
        The original data.
    right_data : np.ndarray
        The data to compare against the `left_data`; this could be another
        copy of the original data or it could be imputed data.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.
    normalisation : Normalisation
        Method of normalising data.  If NONE, no normalisation
        will be used.  If STANDARDISE, then standardise the data by
        dividing by the standard deviation of the `left_data`.  (There
        is no need to subtract the mean, as this does not affect the
        Wasserstein distance.)

    Returns
    -------
    distances : pd.DataFrame
        A DataFrame with columns: split number, direction number and distance
        between the `left_data` in first part of the split and `right_data`
        in the second part of the split in the specified direction.

    """
    distances_list = []
    for split_num, split in enumerate(splits):
        left_split = left_data[split[0], ...]
        right_split = right_data[split[1], ...]
        wdist = WassersteinDistances(left_split, right_split, normalisation)
        distances_list.extend(
            [
                {
                    "split": split_num,
                    "direction": dir_num,
                    "distance": wdist.dir_dist(direction),
                }
                for (dir_num, direction) in enumerate(directions)
            ]
        )
    return pd.DataFrame(distances_list)


def _imputation_distances_for_params(
    args: ImputationArgs,
    params: read_data.ParamDict,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
) -> pd.DataFrame:
    """Calculate imputation distances for specified splits and directions.

    Parameters
    ----------
    args : ImputationArgs
        The parsed commandline arguments.
    params : read_data.ParamDict
        The parameter combination for the experiment of interest.
        See `read_data.DataBundle` for more information.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    Returns
    -------
    distances : pd.DataFrame
        A DataFrame of split number, direction number and distance between the
        original data in first part of the split and imputed data in the second
        part of the split in the specified direction.

    """
    bundle = read_data.DataBundle(args.experiment, args.exclude, params)
    orig_normalised, imputed_normalised = normalise_distributions(
        args.normalise_components, bundle.original_complete, bundle.imputed
    )

    return _imputation_distances_for_datasets(
        orig_normalised,
        imputed_normalised,
        splits,
        directions,
        args.normalise_distances,
    )


def _holdout_distances_for_params(
    args: ImputationArgs,
    params: read_data.ParamDict,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
) -> pd.DataFrame:
    """Calculate baseline distances for specified splits and directions.

    Parameters
    ----------
    args : ImputationArgs
        The parsed commandline arguments.
    params : read_data.ParamDict
        The parameter combination for the experiment of interest.
        See `read_data.DataBundle` for more information.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    Returns
    -------
    distances : pd.DataFrame
        A list of split number, direction number and distance between the
        original data in first part of the split and the second
        part of the split in the specified direction.

    """
    data_reader = read_data.DataReader(args.experiment, args.exclude)
    orig_data = data_reader.get_original_complete_data(params)
    orig_data_normalised = normalise_distribution(args.normalise_components, orig_data)

    return _imputation_distances_for_datasets(
        orig_data_normalised,
        orig_data_normalised,
        splits,
        directions,
        args.normalise_distances,
    )


def _get_directions(
    experiment: str, n_exclude: int, n_directions: int, rand: randoms.Random
) -> list[np.ndarray]:
    """Get random directions for an experiment.

    Parameters
    ----------
    experiment : str
        Either "mimic" or "synthetic".
    n_directions : int
        The number of directions to produce.
    rand : randoms.Random
        An initialised random number generator.

    Returns
    -------
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    """
    num_fields = config.EXPERIMENTS[experiment]["num_fields"] - n_exclude
    directions = [rand.random_direction(num_fields) for _ in range(n_directions)]
    return directions


def _get_marginal_directions(experiment: str) -> list[np.ndarray]:
    """Get marginal directions for an experiment.

    These are just the standard basis vectors.

    Parameters
    ----------
    experiment : str
        Either "mimic" or "synthetic".

    Returns
    -------
    directions : list[np.ndarray]
        A list of standard unit vectors.

    """
    num_fields = config.EXPERIMENTS[experiment]["num_fields"]
    directions = [np.identity(num_fields)[i] for i in range(num_fields)]
    return directions


def _get_splits_for_holdout(
    experiment: str,
    exclude: list[int],
    params: read_data.ParamDict,
    n_splits: int,
    rand: randoms.Random,
) -> list[tuple[np.ndarray, np.ndarray]]:
    """Determine the splits for a given holdout set.

    Parameters
    ----------
    experiment : str
        Either "mimic" or "synthetic".
    exclude : list[int]
        Columns to exclude.
    params : read_data.ParamDict
        The parameter combination for the experiment of interest.
        See `read_data.DataBundle` for more information.
    n_splits : int
        The number of splits of the data.
    rand : randoms.Random
        An initialised random number generator.

    Returns
    -------
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.

    """
    data_reader = read_data.DataReader(experiment, exclude)
    orig_data = data_reader.get_original_complete_data(params)
    n_rows = orig_data.shape[0]
    splits = [rand.random_split_idx(n_rows) for split in range(n_splits)]
    return splits


def _prepend_params_to_dists_df(
    params: read_data.ParamDict, dists: pd.DataFrame
) -> pd.DataFrame:
    """Add parameter columns to the start of a distances DataFrame.

    This function modifies the dists DataFrame in place.

    Parameters
    ----------
    params : read_data.ParamDict
        The parameters used for this set of distances.
    dists : pd.DataFrame
        The set of distances.

    Returns
    -------
    pd.DataFrame
        The resulting combined DataFrame

    """
    for pos, key in enumerate(read_data.PARAM_KEYS):
        dists.insert(pos, key, params[key])
    return dists


def _concat_dists(
    distances: list[tuple[read_data.ParamDict, pd.DataFrame]]
) -> pd.DataFrame:
    """Combine list of parameters+distances into a single DataFrame.

    Parameters
    ----------
    distances : list[tuple[read_data.ParamDict, pd.DataFrame]]
        Each element of this list is a parameter combination and a DataFrame
        of Wasserstein distances relating to these parameters.

    Returns
    -------
    distances_df : pd.DataFrame
        A single DataFrame with extra columns for the parameters.

    """
    distances_dfs = [
        _prepend_params_to_dists_df(params, dists) for (params, dists) in distances
    ]
    return pd.concat(distances_dfs)


def _calculate_baseline_ratios(
    dists: pd.DataFrame, baseline_dists: pd.DataFrame
) -> pd.DataFrame:
    """Calculate the distance/baseline-distance ratios and add to DataFrame.

    Parameters
    ----------
    dists : pd.DataFrame
        Experiment distances.
    baseline_dists : pd.DataFrame
        Baseline distances.

    Returns
    -------
    pd.DataFrame
        This is `dists` with three extra columns: "baseline_distance",
        "distance_ratio", calculated as distance/baseline_distance, and
        "log_distance_ratio", being log(distance_ratio).

    """
    index_keys = ["holdout", "split", "direction"]
    baseline_distances = baseline_dists[index_keys + ["distance"]].rename(
        columns={"distance": "baseline_distance"}
    )
    distances = dists.join(baseline_distances.set_index(index_keys), on=index_keys)
    distances["distance_ratio"] = distances["distance"] / distances["baseline_distance"]
    distances["log_distance_ratio"] = np.log(distances["distance_ratio"])

    return distances


def imputation_distances_for_experiment(
    args: ImputationArgs,
) -> ImputationDistances:
    """Calculate imputation distances for every imputation method.

    This iterates through every parameter combination.  For each one,
    the data is split in two `n_splits` times.  For each split, the
    Wasserstein distance is calculated between the original data in one half
    and the imputed data in the other half in `n_directions` random directions.

    The same calculations are also performed with the original data in one half
    and the original data in the other half as a baseline.

    Parameters
    ----------
    args : ImputationArgs
        The arguments to use for the experiment.

    Returns
    -------
    ImputationDistances
        The calculated distances, including the splits and directions.

    """
    rand = randoms.Random(args.seed)
    directions = _get_directions(
        args.experiment, len(args.exclude), args.n_directions, rand
    )
    # Note that we calculate the random directions even if we're
    # going to overwrite them with the marginal directions, so that
    # the random number generator is in the same state when calculating
    # the splits.
    if args.marginals:
        directions = _get_marginal_directions(args.experiment)

    raw_distances = []
    raw_baseline_distances = []
    holdout_splits: dict[int, list[tuple[np.ndarray, np.ndarray]]] = {}

    # We ensure that we use the same splits for a given holdout set
    for params in read_data.generate_all_params():
        holdout = cast(int, params["holdout"])
        try:
            splits = holdout_splits[holdout]
        except KeyError:
            splits = _get_splits_for_holdout(
                args.experiment, args.exclude, params, args.n_splits, rand
            )
            holdout_splits[holdout] = splits
            raw_baseline_distances.append(
                (
                    params,
                    _holdout_distances_for_params(
                        args,
                        params,
                        splits,
                        directions,
                    ),
                )
            )

        raw_distances.append(
            (
                params,
                _imputation_distances_for_params(
                    args,
                    params,
                    splits,
                    directions,
                ),
            )
        )

    distances = _concat_dists(raw_distances)
    baseline_distances = _concat_dists(raw_baseline_distances)
    distances = _calculate_baseline_ratios(distances, baseline_distances)
    return ImputationDistances(
        distances=distances,
        baseline_distances=baseline_distances,
        holdout_splits=holdout_splits,
        directions=directions,
    )


def _save_distances_to_csv(
    distances: pd.DataFrame,
    filename: Union[str, Path],
) -> None:
    """Write the calculated imputation distances to a CSV file.

    Parameters
    ----------
    distances : pd.DataFrame
        Calculated distances, including the split number and direction number,
        for example using `imputation_distances_for_experiment()`.
    filename : str
        CSV filename to write to.

    Returns
    -------
    None.

    """
    distances.to_csv(filename, index=False)


def _save_splits_to_json(
    holdout_splits: dict[int, list[tuple[np.ndarray, np.ndarray]]],
    filename: Union[str, Path],
) -> None:
    """Write the holdout splits to a file in JSON format.

    Note that we cannot easily save this to a CSV file as the sizes of
    the splits may differ between holdout sets.  We prefer JSON over pickle
    as it is human-readable.

    Parameters
    ----------
    holdout_splits: dict[int, list[tuple[np.ndarray, np.ndarray]]]
        For each holdout set, a list of splits.
    filename : str
        Filename to write to.

    Returns
    -------
    None.

    """
    # We can't serialise an np.ndarray directly; we have to convert it
    # into a regular Python list first.
    holdout_serialisable = {
        holdout: {
            splitnum: (split[0].tolist(), split[1].tolist())
            for splitnum, split in enumerate(splits)
        }
        for holdout, splits in holdout_splits.items()
    }
    with open(filename, "w", encoding="UTF-8") as fp:
        json.dump(holdout_serialisable, fp)


def _save_directions_to_csv(
    directions: list[np.ndarray],
    filename: Union[str, Path],
) -> None:
    """Write the directions to a CSV file.

    Parameters
    ----------
    directions: list[np.ndarray]
        The directions used for projections.
    filename : str
        Filename to write to.

    Returns
    -------
    None.

    """
    num_columns = directions[0].size
    columns = [f"var {var}" for var in range(1, num_columns + 1)]
    directions_pd = pd.DataFrame(directions, columns=columns)
    directions_pd.to_csv(filename, index=True, index_label="direction")


def _extra_filename_str(args: ImputationArgs) -> str:
    """Determine an extra string to add to filenames based on commandline

    Parameters
    ----------
    args : ImputationArgs
        Commandline arguments.

    Returns
    -------
    str
        Includes "marginals" if --marginals is requested, and "standardise"
        if --data_standardise=standardise is requested.

    """

    extra_str = ""
    if args.marginals:
        extra_str += "marginals-"
    if args.normalise_components == Normalisation.STANDARDISE:
        extra_str += "compnorm-"
    if args.normalise_distances == Normalisation.NONE:
        extra_str += "nodistnorm-"
    return extra_str


def calculate_and_save_experiment_distances(args: ImputationArgs) -> None:
    """Calculate and save imputation and baseline distances for an experiment.

    The directory in which to save the results is determined from the config
    file.

    Parameters
    ----------
    args : ImputationArgs
        The arguments to use for the experiment.

    Returns
    -------
    None

    """
    imp_results = imputation_distances_for_experiment(args)
    output_dir = config.Config().get_dir_output()
    extra_str = _extra_filename_str(args)
    _save_distances_to_csv(
        imp_results.distances,
        output_dir / f"{args.experiment}-{extra_str}distances.csv",
    )
    _save_distances_to_csv(
        imp_results.baseline_distances,
        output_dir / f"{args.experiment}-{extra_str}baseline-distances.csv",
    )
    _save_splits_to_json(
        imp_results.holdout_splits,
        output_dir / f"{args.experiment}-{extra_str}splits.json",
    )
    _save_directions_to_csv(
        imp_results.directions,
        output_dir / f"{args.experiment}-{extra_str}directions.csv",
    )
