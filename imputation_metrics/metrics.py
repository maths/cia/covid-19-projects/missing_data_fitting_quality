#!/usr/bin/env python3

"""Calculate measures of distance between imputed data and original data."""

import json
from dataclasses import dataclass
from pathlib import Path
from typing import Optional, Union

import numpy as np
import pandas as pd  # type: ignore
from typed_argparse import TypedArgs

from . import randoms
from .normalisation import (Normalisation, normalise_distribution,
                            normalise_distributions)
from .wasserstein import WassersteinDistances


class WassersteinArgs(TypedArgs):
    """Arguments for imputation distance calculations.

    n_splits : int
        The number of splits of the data.
    n_directions : int
        The number of random directions to calculate distances in per
        simulation.
    marginals : bool
        Use marginals instead of random directions.
    normalise_components : Normalisation
        Method of normalising given data.  If NONE, no normalisation will be
        used.  If STANDARDISE, then for each column (variable), normalise both
        the original data and imputed data using the mean and standard
        deviation of the original data.
    normalise_distances : Normalisation
        Method of normalising data when calculating Wasserstein distance
        between two data vectors.  If NONE, no normalisation will be
        used.  If STANDARDISE, then standardise the data by dividing by
        the standard deviation of the original data.  (There is no need to
        subtract the mean, as this does not affect the Wasserstein distance.)
    seed : Optional[int]
        The seed to use for the random direction and split generator.
        The same set of directions and splits will be used for all of the
        parameter combinations (though the splits will be different for
        each holdout set).

    """

    n_splits: int
    n_directions: int
    marginals: bool
    normalise_components: Normalisation
    normalise_distances: Normalisation
    seed: Optional[int]


@dataclass
class ImputationDistances:
    """Hold the results of an imputation distances computation"""

    distances: pd.DataFrame
    baseline_distances: pd.DataFrame
    splits: list[tuple[np.ndarray, np.ndarray]]
    directions: list[np.ndarray]


def _imputation_distances_for_datasets(
    left_data: np.ndarray,
    right_data: np.ndarray,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
    normalisation: Normalisation,
) -> pd.DataFrame:
    """Calculate imputation distances for specified splits and directions.

    This takes two given sets of data and calculates the imputation distances
    using the specified splits in the specified directions.

    The `left_data` is regarded as the original data set and the `right_data`
    is the data being compared to it.  These two data sets must have the
    same number of rows; the pairs in the list of splits are indices into this
    data.

    Parameters
    ----------
    left_data : np.ndarray
        The original data.
    right_data : np.ndarray
        The data to compare against the `left_data`; this could be another
        copy of the original data or it could be imputed data.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.
    normalisation : Normalisation
        Method of normalising data.  If NONE, no normalisation
        will be used.  If STANDARDISE, then standardise the data by
        dividing by the standard deviation of the `left_data`.  (There
        is no need to subtract the mean, as this does not affect the
        Wasserstein distance.)

    Returns
    -------
    distances : pd.DataFrame
        A DataFrame with columns: split number, direction number and distance
        between the `left_data` in first part of the split and `right_data`
        in the second part of the split in the specified direction.

    """
    distances_list = []
    for split_num, split in enumerate(splits):
        left_split = left_data[split[0], ...]
        right_split = right_data[split[1], ...]
        wdist = WassersteinDistances(left_split, right_split, normalisation)
        distances_list.extend(
            [
                {
                    "split": split_num,
                    "direction": dir_num,
                    "distance": wdist.dir_dist(direction),
                }
                for (dir_num, direction) in enumerate(directions)
            ]
        )
    return pd.DataFrame(distances_list)


def _distances_for_datasets(
    original_data: np.ndarray,
    imputed_data: np.ndarray,
    args: WassersteinArgs,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
) -> pd.DataFrame:
    """Calculate imputation distances for specified splits and directions.

    Parameters
    ----------
    original_data : np.ndarray
        The original data.
    imputed_data : np.ndarray
        The imputed data.
    args : WassersteinArgs
        The parsed commandline arguments.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    Returns
    -------
    distances : pd.DataFrame
        A DataFrame of split number, direction number and distance between the
        original data in first part of the split and imputed data in the second
        part of the split in the specified direction.

    """
    orig_normalised, imputed_normalised = normalise_distributions(
        args.normalise_components, original_data, imputed_data
    )

    return _imputation_distances_for_datasets(
        orig_normalised,
        imputed_normalised,
        splits,
        directions,
        args.normalise_distances,
    )


def _baseline_distances_for_dataset(
    original_data: np.ndarray,
    args: WassersteinArgs,
    splits: list[tuple[np.ndarray, np.ndarray]],
    directions: list[np.ndarray],
) -> pd.DataFrame:
    """Calculate baseline distances for specified splits and directions.

    Parameters
    ----------
    original_data : np.ndarray
        The original data.
    args : WassersteinArgs
        The parsed commandline arguments.
    splits: list[tuple[np.ndarray, np.ndarray]]
        Each pair in this list specifies one split of the data.  Each of the
        entries in the pair is a 1-dimensional array giving the indices for
        the left or right data respectively.
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    Returns
    -------
    distances : pd.DataFrame
        A list of split number, direction number and distance between the
        original data in first part of the split and the second
        part of the split in the specified direction.

    """
    orig_data_normalised = normalise_distribution(
        args.normalise_components, original_data
    )

    return _imputation_distances_for_datasets(
        orig_data_normalised,
        orig_data_normalised,
        splits,
        directions,
        args.normalise_distances,
    )


def _get_directions(
    dimension: int, n_directions: int, rand: randoms.Random
) -> list[np.ndarray]:
    """Get random directions for an experiment.

    Parameters
    ----------
    dimension : int
        Number of fields in the data (its dimensionality).
    n_directions : int
        The number of directions to produce.
    rand : randoms.Random
        An initialised random number generator.

    Returns
    -------
    directions : list[np.ndarray]
        A list of unit vectors specifying the directions to use.  The
        results will be given in the same order.

    """
    directions = [rand.random_direction(dimension) for _ in range(n_directions)]
    return directions


def _get_marginal_directions(dimension: int) -> list[np.ndarray]:
    """Get marginal directions for an experiment.

    These are just the standard basis vectors.

    Parameters
    ----------
    dimension : int
        Number of fields in the data (its dimensionality).

    Returns
    -------
    directions : list[np.ndarray]
        A list of standard unit vectors.

    """
    directions = [np.identity(dimension)[i] for i in range(dimension)]
    return directions


def _calculate_baseline_ratios(
    dists: pd.DataFrame, baseline_dists: pd.DataFrame
) -> pd.DataFrame:
    """Calculate the distance/baseline-distance ratios and add to DataFrame.

    Parameters
    ----------
    dists : pd.DataFrame
        Imputation distances.
    baseline_dists : pd.DataFrame
        Baseline distances.

    Returns
    -------
    pd.DataFrame
        This is `dists` with three extra columns: "baseline_distance",
        "distance_ratio", calculated as distance/baseline_distance, and
        "log_distance_ratio", being log(distance_ratio).

    """
    index_keys = ["split", "direction"]
    baseline_distances = baseline_dists[index_keys + ["distance"]].rename(
        columns={"distance": "baseline_distance"}
    )
    distances = dists.merge(baseline_distances, on=index_keys)
    distances["distance_ratio"] = distances["distance"] / distances["baseline_distance"]
    distances["log_distance_ratio"] = np.log(distances["distance_ratio"])

    return distances


def imputation_distances_from_data(
    original_data: np.ndarray, imputed_data: np.ndarray, args: WassersteinArgs
) -> ImputationDistances:
    """Calculate imputation distances for every imputation method.

    This iterates through every parameter combination.  For each one,
    the data is split in two `n_splits` times.  For each split, the
    Wasserstein distance is calculated between the original data in one half
    and the imputed data in the other half in `n_directions` random directions.

    The same calculations are also performed with the original data in one half
    and the original data in the other half as a baseline.

    Parameters
    ----------
    original_data : np.ndarray
        The original data.
    imputed_data : np.ndarray
        The imputed data.
    args : WassersteinArgs
        The arguments to use for the experiment.

    Returns
    -------
    ImputationDistances
        The calculated distances, including the splits and directions.

    """
    rand = randoms.Random(args.seed)
    n_rows, dimension = original_data.shape
    directions = _get_directions(dimension, args.n_directions, rand)
    # Note that we calculate the random directions even if we're
    # going to overwrite them with the marginal directions, so that
    # the random number generator is in the same state when calculating
    # the splits.
    if args.marginals:
        directions = _get_marginal_directions(dimension)

    splits = [rand.random_split_idx(n_rows) for split in range(args.n_splits)]

    baseline_distances = _baseline_distances_for_dataset(
        original_data,
        args,
        splits,
        directions,
    )

    distances = _distances_for_datasets(
        original_data,
        imputed_data,
        args,
        splits,
        directions,
    )

    distances = _calculate_baseline_ratios(distances, baseline_distances)

    return ImputationDistances(
        distances=distances,
        baseline_distances=baseline_distances,
        splits=splits,
        directions=directions,
    )


def _save_distances_to_csv(
    distances: pd.DataFrame,
    filename: Union[str, Path],
) -> None:
    """Write the calculated imputation distances to a CSV file.

    Parameters
    ----------
    distances : pd.DataFrame
        Calculated distances, including the split number and direction number,
        for example using `imputation_distances_for_experiment()`.
    filename : str
        CSV filename to write to.

    Returns
    -------
    None.

    """
    distances.to_csv(filename, index=False)


def _save_splits_to_json(
    splits: list[tuple[np.ndarray, np.ndarray]],
    filename: Union[str, Path],
) -> None:
    """Write the holdout splits to a file in JSON format.

    Note that we cannot easily save this to a CSV file as we want to ensure
    it is clear where one split ends and the other begins.  We prefer JSON
    over pickle as it is human-readable.

    Parameters
    ----------
    splits: list[tuple[np.ndarray, np.ndarray]]
        List of splits.
    filename : str
        Filename to write to.

    Returns
    -------
    None.

    """
    # We can't serialise an np.ndarray directly; we have to convert it
    # into a regular Python list first.
    serialisable = {
        splitnum: (split[0].tolist(), split[1].tolist())
        for splitnum, split in enumerate(splits)
    }
    with open(filename, "w", encoding="UTF-8") as fp:
        json.dump(serialisable, fp)


def _save_directions_to_csv(
    directions: list[np.ndarray],
    filename: Union[str, Path],
) -> None:
    """Write the directions to a CSV file.

    Parameters
    ----------
    directions: list[np.ndarray]
        The directions used for projections.
    filename : str
        Filename to write to.

    Returns
    -------
    None.

    """
    num_columns = directions[0].size
    columns = [f"var {var}" for var in range(1, num_columns + 1)]
    directions_pd = pd.DataFrame(directions, columns=columns)
    directions_pd.to_csv(filename, index=True, index_label="direction")


def calculate_and_save_wasserstein_distances(
    original_data: np.ndarray,
    imputed_data: np.ndarray,
    output_prefix: str,
    args: WassersteinArgs,
) -> None:
    """Calculate and save imputation and baseline distances for an experiment.

    Parameters
    ----------
    original_data : np.ndarray
        The original data.
    imputed_data : np.ndarray
        The imputed data.
    args : WassersteinArgs
        The arguments to use for the distance calculations.
    output_prefix : str
        The filename prefix to use when saving the results.

    Returns
    -------
    None

    """
    imp_results = imputation_distances_from_data(original_data, imputed_data, args)
    _save_distances_to_csv(
        imp_results.distances,
        f"{output_prefix}-distances.csv",
    )
    _save_distances_to_csv(
        imp_results.baseline_distances,
        f"{output_prefix}-baseline-distances.csv",
    )
    _save_splits_to_json(
        imp_results.splits,
        f"{output_prefix}-splits.json",
    )
    _save_directions_to_csv(
        imp_results.directions,
        f"{output_prefix}-directions.csv",
    )
