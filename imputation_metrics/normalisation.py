"""Utilities for Wasserstein distance normalisation"""

from enum import Enum

import numpy as np


class Normalisation(Enum):
    """What type of normalisation to use"""

    NONE = "none"
    STANDARDISE = "standardise"


def _get_standardisation_parameters(
    data: np.ndarray,
) -> tuple[np.ndarray, np.ndarray]:
    """Return the mean and standard deviation of each column in the data.

    Parameters
    ----------
    data : np.ndarray
        Data array.

    Returns
    -------
    (mean, sd) : (np.ndarray, np.ndarray)
        Two 1-D ndarrays with the mean and standard deviations of the columns
        of `data`.

    """

    mean = np.mean(data, axis=0)
    sd = np.std(data, axis=0)
    return (mean, sd)


def normalise_distribution(
    norm: Normalisation, dist: np.ndarray
) -> np.ndarray:
    """Normalise every variable in the distribution as requested in `norm`.

    If `norm` == NONE, then return the data unchanged.

    If `norm` == STANDARDISE, then normalise each column to have mean 0,
    standard deviation 1.

    Parameters
    ----------
    norm : Normalisation
        The type of normalisation to apply.
    dist : np.ndarray
        The data distribution to be normalised.

    Returns
    -------
    normalised : np.ndarray
        The normalised data.

    """
    if norm == Normalisation.NONE:
        return dist
    if norm == Normalisation.STANDARDISE:
        mean, sd = _get_standardisation_parameters(dist)
        return (dist - mean) / sd
    raise ValueError(f"Unrecognised Normalisation value: {norm}")


def normalise_distributions(
    norm: Normalisation, orig: np.ndarray, other: np.ndarray
) -> tuple[np.ndarray, np.ndarray]:
    """Normalise every variable in the distributions as requested in `norm`.

    If `norm` == NONE, then return the data unchanged.

    If `norm` == STANDARDISE, then normalise each column in `orig` to have
    mean 0, standard deviation 1, and normalise each column in `other` using
    the mean and standard deviations of the columns in `orig`.

    Parameters
    ----------
    norm : Normalisation
        The type of normalisation to apply.
    orig : np.ndarray
        The original data distribution to be normalised.
    other : np.ndarray
        A second data distribution to be normalised using the parameters from
        the first distribution.

    Returns
    -------
    orig_normalised : np.ndarray
        The normalised original data.
    other_normalised : np.ndarray
        The normalised other data.

    """
    if norm == Normalisation.NONE:
        return orig, other
    if norm == Normalisation.STANDARDISE:
        mean, sd = _get_standardisation_parameters(orig)
        orig_normed = (orig - mean) / sd
        other_normed = (other - mean) / sd
        return orig_normed, other_normed
    raise ValueError(f"Unrecognised Normalisation value: {norm}")
