#!/usr/bin/env python3

"""Read the configuration file, and specify other project-wide constants."""

import configparser
import os
from pathlib import Path

# Constants that are used in the package, all collected here for easy
# reference.

# Most of the experiments use datasets with already-missing values.
# Here, we are only evaluating those experiments with artificially
# missing values.
# The "num_fields" value is the number of fields in the dataset.
EXPERIMENTS = {
    "mimic": {"num_fields": 14},
    "synthetic": {"num_fields": 25},
    "syncat": {"num_fields": 28},
    "syncat_thresholded": {"num_fields": 28},
}
# These are the imputation methods explored.
IMPUTERS = ["GAIN", "Mean", "MICE", "MissForest", "MIWAE"]
# Though there are files in the original data with train and/or test
# missingness set to 0, these are not used in the experiments.
MISSINGNESS = [(0.25, 0.25), (0.25, 0.5), (0.5, 0.25), (0.5, 0.5)]
# The size of the per-holdout cross validation
VALIDATIONS = 5
# The number of holdout sets
HOLDOUTS = 3
# The number of experiments (repeats) on each of the above combinations
REPEATS = 10
# The total number of experiments on a given dataset
TOTAL_PER_EXPERIMENT = (
    len(IMPUTERS) * len(MISSINGNESS) * VALIDATIONS * HOLDOUTS * REPEATS
)
# The total number of experiments on a given holdout dataset
TOTAL_PER_HOLDOUT = len(IMPUTERS) * len(MISSINGNESS) * VALIDATIONS * REPEATS

# How extreme data can be before it is considered an outlier
# This is measured in units of interquartile range (IQR), anything more
# than this multiple of the IQR above or below the median is considered
# to be an outlier.
EXTREME_IQR = 10
# If we use mean +/- multiple of s.d. as the limit of extreme values, then
# we use this multiple of s.d.
EXTREME_SD = 10


class Config:
    """Read the configuration data from "config.toml"."""

    def __init__(self) -> None:
        self.config = self._find_and_read_config()
        for collection in ["original", "imputed"]:
            if collection not in self.config.sections():
                raise ValueError(f"No '{collection}' section in config.ini")

    def get_dir_original(self, experiment: str) -> Path:
        """Get the directory of the original (complete) data for an experiment.

        Parameters
        ----------
        experiment : str
            Should be either "mimic" or "synthetic".

        Returns
        -------
        Path
            The directory location.

        """
        try:
            orig_dir = Path(self.config["original"][experiment])
        except KeyError:
            # pylint: disable=raise-missing-from
            raise KeyError(f"Original {experiment} directory not found in config.ini")

        if not orig_dir.exists():
            raise FileNotFoundError(f"Original {experiment} directory not found")

        return orig_dir

    def get_dir_imputed(self, experiment: str) -> Path:
        """Get the directory of the imputed data for an experiment.

        Parameters
        ----------
        experiment : str
            Should be either "mimic" or "synthetic".

        Returns
        -------
        Path
            The directory location.

        """
        try:
            imputed_dir = Path(self.config["imputed"][experiment])
        except KeyError:
            # pylint: disable=raise-missing-from
            raise KeyError(f"Imputed {experiment} directory not found in config.ini")

        if not imputed_dir.exists():
            raise FileNotFoundError(f"Imputed {experiment} directory not found")

        return imputed_dir

    def get_dir_output(self) -> Path:
        """Get the directory for output.

        Returns
        -------
        Path
            The output directory location.

        """
        try:
            output_dir = Path(self.config["output"]["directory"])
        except KeyError:
            # pylint: disable=raise-missing-from
            raise KeyError("Output directory not found in config.ini")

        output_dir.mkdir(parents=True, exist_ok=True)

        return output_dir

    @staticmethod
    def _find_and_read_config() -> configparser.ConfigParser:
        parser = configparser.ConfigParser()

        # In some contexts, __file__ is undefined, so we give it a reasonable
        # default value
        try:
            module_dirname = os.path.dirname(__file__)
        except NameError:
            module_dirname = os.curdir

        for loc in [
            os.curdir,
            os.pardir,
            module_dirname,
            os.path.join(module_dirname, os.pardir),
        ]:
            configfile = os.path.join(loc, "config.ini")
            if parser.read(configfile):
                return parser

        raise FileNotFoundError("Could not find 'config.ini'")
