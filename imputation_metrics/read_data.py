"""Read original and imputed data from the MIMIC or SYNTHETIC datasets.

This module provides three classes and some convenience functions:

* DataReader: read the original and imputed datasets from a specific
    experiment.

* DataBundle: holds original and imputed data from a specified experiment.
    This class also provides functions to sanitise the data.

* ExtremeData: interrogates the data collections to identify extreme data
    values.  The results of this can be used by the DataBundle functions
    to remove this anomolous data before further processing.

* Convenience functions:

    - get_quartiles; iqr_normalise_data: find the median and IQR;
      normalise the data using these.
    - get_mean_sd; normalise_data: find the mean and standard deviation,
      normalise the data using these.
    - generate_all_params: an interator to work over all experiments for
      a given original dataset (MIMIC or Synthetic).
    - check_all_imputation_validities: check that every imputed dataset
      matches the original dataset in places where the original data was
      present.
    - find_all_missing_imputation_cells: identify all cells which have
      NA in at least one imputation experiment.

"""

from typing import Iterator, Union

import numpy as np

from . import config

ParamDict = dict[str, Union[str, int, float]]

PARAM_KEYS = [
    "imputation",
    "holdout",
    "train_missing",
    "test_missing",
    "validation",
    "repeat",
]


class DataReader:
    """Read data from imputation experiments.

    Parameters
    ----------
    experiment : str
        Either "mimic", "synthetic", "syncat" or "syncat_thresholded".
    exclude : list[int]
        Columns to exclude.

    """

    def __init__(self, experiment: str, exclude: list[int]) -> None:
        pathconfig = config.Config()
        self.experiment = experiment
        self.exclude = exclude
        self.original_dir = pathconfig.get_dir_original(experiment)
        self.imputed_dir = pathconfig.get_dir_imputed(experiment)
        self.num_fields = config.EXPERIMENTS[experiment]["num_fields"]

    def get_original_complete_data(self, params: ParamDict) -> np.ndarray:
        """Return the complete data for this holdout (test) set.

        This reads a file such as
        "SYNTHETIC/holdout_0_train_missing_0_test_missing_0.csv"
        These complete data sets are not used for training or testing purposes,
        but conveniently have the complete data.

        The returned ndarray only has the data columns, not the index or
        "expired" (MIMIC) or "output" (Synthetic) columns.

        Parameters
        ----------
        params : ParamDict
            Dictionary of parameters, containing at least the following key:

            * holdout : int
              The holdout set, should be 0, 1 or 2.

        Returns
        -------
        data : np.ndarray
            The complete data.

        """
        filename = f"holdout_{params['holdout']}_train_missing_0_test_missing_0.csv"
        full_path = self.original_dir / filename
        fields = range(1, self.num_fields + 1)
        full_data = np.loadtxt(full_path, delimiter=",", skiprows=1, usecols=fields)
        return np.delete(full_data, self.exclude, axis=1)

    def get_original_missing_data(self, params: ParamDict) -> np.ndarray:
        """Return the holdout (test) set with missing data.

        This reads a file such as
        "SYNTHETIC/holdout_0_train_missing_0.25_test_missing_0.5.csv"
        where the fractions missing in the training and testing are specified.

        The returned ndarray only has the data columns, not the index or
        "expired" (MIMIC) or "output" (Synthetic) columns.

        Note that the `train_fraction` is not actually needed, as the same
        data is missing from the holdout set regardless of the `train_fraction`
        value, but as there are multiple identical holdout set CSVs for the
        different `train_fraction` values, we err on the side of caution and
        load the specified one.

        Parameters
        ----------
        params : ParamDict
            Dictionary of parameters, containing at least the following keys:

            * "holdout" : int
              The holdout set, should be 0, 1 or 2.
            * "train_missing" : float
              The fraction of data missing for training.
            * "test_missing" : float
              The fraction of data missing for testing (i.e., from the
              holdout set).

        Returns
        -------
        data : np.ndarray
            The holdout set with missing data recorded as `numpy.nan`.

        """
        filename = (
            f"holdout_{params['holdout']}_"
            f"train_missing_{params['train_missing']}_"
            f"test_missing_{params['test_missing']}.csv"
        )
        full_path = self.original_dir / filename
        fields = range(1, self.num_fields + 1)
        full_data = np.genfromtxt(
            full_path,
            delimiter=",",
            skip_header=1,
            missing_values="",
            filling_values=np.nan,
            usecols=fields,
        )
        return np.delete(full_data, self.exclude, axis=1)

    def get_imputed_data(self, params: ParamDict) -> np.ndarray:
        """Return the holdout (test) set with missing data imputed.

        This looks in a directory such as
        "MIMIC/MICE/hparam_-1/train_per_0.25/test_per_0.5/holdout_0.0/val_0.0/m_1"
        and returns the "imputed_test_x.npy" or "imputed_test_x.csv" file
        stored there as an ndarray.

        Parameters
        ----------
        params : ParamDict
            Dictionary of parameters, containing at least the following keys:

            * "imputation" : str
              The method of imputation used.
            * "holdout" : int
              The holdout set, should be 0, 1 or 2.
            * "train_missing" : float
              The fraction of data missing for training.
            * "test_missing" : float
              The fraction of data missing for testing (i.e., from the
              holdout set).
            * "validation" : int
              Which cross-validation fold to use, should be 0, 1, 2, 3 or 4.
            * "repeat" : int
              Which of the 10 repetitions of the experiment to use.
              Should lie in the range 0 to 9 (inclusive).

        Returns
        -------
        data : np.ndarray
            The holdout (test) set with imputed data.

        """
        # We are looking for files with names such as (split over two lines):
        #   MIMIC/MIWAE/hparam_-1/train_per_0.25/test_per_0.25/
        #    holdout_0.0/val_0.0/m_1/imputed_test_x.npy
        full_dir = (
            self.imputed_dir
            / f"{params['imputation']}"
            / f"train_per_{params['train_missing']}"
            / f"test_per_{params['test_missing']}"
            / f"holdout_{params['holdout']}"
            / f"val_{params['validation']}"
            / f"m_{params['repeat']}"
        )
        full_path = full_dir / "imputed_test_x.npy"
        data = np.load(full_path)

        return np.delete(data, self.exclude, axis=1)


class DataBundle:
    """Hold original and imputed data from an imputation experiment.

    Parameters
    ----------
    experiment : str
        Either "mimic", "synthetic", "syncat" or "syncat_thresholded".
    exclude : list[int]
        Columns to exclude.
    params : ParamDict
        Dictionary of parameters specifying the imputation experiment.
        It must contain at least the following keys:

        * "imputation" : str
          The method of imputation used.
        * "holdout" : int
          The holdout set, should be 0, 1 or 2.
        * "train_missing" : float
          The fraction of data missing for training.
        * "test_missing" : float
          The fraction of data missing for testing (i.e., from the
          holdout set).
        * "validation" : int
          Which cross-validation fold to use, should be 0, 1, 2, 3 or 4.
        * "repeat" : int
          Which of the 10 repetitions of the experiment to use.
          Should lie in the range 0 to 9 (inclusive).

    """

    def __init__(self, experiment: str, exclude: list[int], params: ParamDict) -> None:
        reader = DataReader(experiment, exclude)
        self.experiment = experiment
        self.params = params
        self.original_complete = reader.get_original_complete_data(params)
        self.original_missing = reader.get_original_missing_data(params)
        self.imputed = reader.get_imputed_data(params)

    def get_imputed_values(self, feature: int) -> np.ndarray:
        """Get a vector of the imputed values for a specific feature.

        This function identifies which entries of a feature have been
        imputed and returns only those.  The result is a vector which
        will generally be shorter than the number of rows in the original
        data.

        Parameters
        ----------
        feature : int
            The feature number to use.

        Returns
        -------
        imputed_values : np.ndarray
            The imputed data values.

        """
        original_missing_feature = self.original_missing[:, feature]
        imputed_feature = self.imputed[:, feature]
        missing_coords = np.isnan(original_missing_feature)
        imputed_values = imputed_feature[missing_coords]
        return imputed_values

    def locate_missing_imputed_data(self) -> np.ndarray:
        """Locate all cells with missing imputed data.

        Returns
        -------
        missing : np.ndarray
            List of cells with missing imputed data.

        """
        return np.argwhere(np.isnan(self.imputed))

    def check_imputation_validity(self) -> bool:
        """Check the imputation has left the already-present data unchanged.

        Returns
        -------
        bool
            Has the original data been left unchanged by the imputation,
            and has every piece of missing data been imputed?

        """
        # Fill the missing data with the imputed data so that it compares
        # equal; we only wish to compare the non-imputed data.
        filled_missing = np.where(
            np.isnan(self.original_missing),
            self.imputed,
            self.original_missing,
        )
        missing_close_to_imputed = np.isclose(filled_missing, self.imputed)
        return bool(np.all(missing_close_to_imputed))

    def sanitise_data(self) -> None:
        """Remove data rows with missing imputed data.

        This is necessary if some imputation fails to impute some data
        values.  This occurred with the original broken MIMIC dataset.

        This function removes the relevant rows in all of the arrays.

        Returns
        -------
        None

        """
        missing_items = np.argwhere(np.isnan(self.imputed))
        missing_rows = np.unique(missing_items[:, 0])
        if missing_rows.size > 0:
            for data in ["original_complete", "original_missing", "imputed"]:
                sanitised_data = np.delete(getattr(self, data), missing_rows, axis=0)
                setattr(self, data, sanitised_data)

    def delete_extreme_rows(
        self, extreme_rows: Union[list[list[int]], list[np.ndarray]]
    ) -> None:
        """Delete rows containing extreme data.

        This function removes the relevant rows in all of the arrays.

        Parameters
        ----------
        extreme_rows : list[Union[list[int], np.ndarray]], optional
            If provided, should be a list of lists of ints or ndarrays,
            each list of ints or ndarray being a 1-dimensional list of
            rows to delete, one list per holdout set.  This could be,
            for example, the output of an
            `ExtremeData(experiment).locate_*extreme_rows()` method.
            If it is not provided,
            `ExtremeData(experiment).locate_iqr_extreme_rows()` will be
            used.

        Returns
        -------
        None

        """
        self._delete_rows(extreme_rows[int(self.params["holdout"])])

    def _delete_rows(self, rows_to_delete: Union[list[int], np.ndarray]) -> None:
        """Delete specified rows from the data.

        This function removes the relevant rows in all of the arrays.

        Returns
        -------
        None

        """
        for data in ["original_complete", "original_missing", "imputed"]:
            sanitised_data = np.delete(getattr(self, data), rows_to_delete, axis=0)
            setattr(self, data, sanitised_data)


class ExtremeData:
    """Identify extreme data in the original datasets.

    This class contains methods to identify extreme data in different ways.
    The configuration options controlling the limits of what is considered
    extreme are defined in "config.py"; they are
    config.EXTREME_IQR and config.EXTREME_SD.

    Parameters
    ----------
    experiment : str
        Either "mimic", "synthetic", "syncat" or "syncat_thresholded".
    exclude : list[int]
        Columns to exclude.

    """

    def __init__(self, experiment: str, exclude: list[int]) -> None:
        # We store the original (complete) experiment data in
        # `original_data`.
        reader = DataReader(experiment, exclude)
        self.original_data = []
        for holdout in range(config.HOLDOUTS):
            params: ParamDict = {"holdout": holdout}
            data = reader.get_original_complete_data(params)
            self.original_data.append(data)
        # We also determine per-variable quartiles etc.
        complete_data = np.concatenate(self.original_data)
        self.quartiles = get_quartiles(complete_data)
        self.mean_sd = get_mean_sd(complete_data)

    def iqr_normalised_data(self) -> list[np.ndarray]:
        """Calculate the data normalised to multiples of IQR above median.

        Returns
        -------
        normalised_data : list[np.ndarray]
            The normalised data.

        """
        normalised_data = [
            iqr_normalise_data(orig_data, self.quartiles)
            for orig_data in self.original_data
        ]
        return normalised_data

    def normalised_data(self) -> list[np.ndarray]:
        """Calculate the data normalised to multiples of s.d. above mean.

        Returns
        -------
        normalised_data : list[np.ndarray]
            The normalised data.

        """
        normalised_data = [
            normalise_data(orig_data, self.mean_sd) for orig_data in self.original_data
        ]
        return normalised_data

    def locate_iqr_extreme_cells(self) -> list[np.ndarray]:
        """Locate the cells containing extreme data based on IQR.

        Here, any data more than `config.EXTREME_IQR` multiples of the IQR
        above or below the median is considered extreme.  The IQR and median
        are calculated for each variable separately, but are calculated across
        all holdout sets combined.

        This function returns the extreme cells in each holdout set separately.

        Returns
        -------
        extreme_cells : list[np.ndarray]
            The cell locations containing extreme data, one list per holdout
            set.

        """
        return self._locate_extreme_cells_generic(self.quartiles)

    def locate_extreme_cells(self) -> list[np.ndarray]:
        """Locate the cells containing extreme data based on mean/s.d.

        Here, any data more than `config.EXTREME_SD` multiples of the s.d.
        above or below the mean is considered extreme.  The mean and s.d.
        are calculated for each variable separately, but are calculated across
        all holdout sets combined.

        This function returns the extreme cells in each holdout set separately.

        Returns
        -------
        extreme_cells : list[np.ndarray]
            The cell locations containing extreme data, one list per holdout
            set.

        """
        return self._locate_extreme_cells_generic(self.mean_sd)

    def locate_iqr_extreme_rows(self) -> list[np.ndarray]:
        """Locate the rows containing extreme data based on IQR.

        Here, any data more than `config.EXTREME_IQR` multiples of the IQR
        above or below the median is considered extreme.  The IQR and median
        are calculated for each variable separately, but are calculated across
        all holdout sets combined.

        This function returns the rows in each holdout set separately.

        Returns
        -------
        extreme_rows : list[np.ndarray]
            The rows containing extreme data, one list (as a one-dimensional
            ndarray) per holdout set.

        """
        extreme_cells = self.locate_iqr_extreme_cells()
        return self._cells_to_unique_rows(extreme_cells)

    def locate_extreme_rows(self) -> list[np.ndarray]:
        """Locate the rows containing extreme data based on mean/s.d.

        Here, any data more than `config.EXTREME_SD` multiples of the s.d.
        above or below the mean is considered extreme.  The mean and s.d.
        are calculated for each variable separately, but are calculated across
        all holdout sets combined.

        This function returns the rows in each holdout set separately.

        Returns
        -------
        extreme_rows : list[np.ndarray]
            The rows containing extreme data, one list (as a one-dimensional
            ndarray) per holdout set.

        """
        extreme_cells = self.locate_extreme_cells()
        return self._cells_to_unique_rows(extreme_cells)

    def get_extreme_iqr_normalised_values(self) -> list[np.ndarray]:
        """Determine the IQR-normalised values in extreme cells.

        Returns
        -------
        normalised_values : list[np.ndarray]
            The IQR-normalised values of extreme cells, one holdout set
            at a time.

        """
        extreme_cells = self.locate_iqr_extreme_cells()
        normalised_data = self.iqr_normalised_data()
        return self._get_selected_values(normalised_data, extreme_cells)

    def get_extreme_normalised_values(self) -> list[np.ndarray]:
        """Determine the sd-normalised values in extreme cells.

        Returns
        -------
        normalised_values : list[np.ndarray]
            The IQR-normalised values of extreme cells, one holdout set
            at a time.

        """
        extreme_cells = self.locate_extreme_cells()
        normalised_data = self.normalised_data()
        return self._get_selected_values(normalised_data, extreme_cells)

    def _locate_extreme_cells_generic(
        self, limits: dict[str, np.ndarray]
    ) -> list[np.ndarray]:
        """Locate the cells containing extreme data based on given limits.

        This function returns the extreme cells in each holdout set separately.

        Parameters
        ----------
        limits : dict[str, np.ndarray]
            A dictionary with keys "upper" and "lower" giving the per-variable
            limits on what is considered extreme.

        Returns
        -------
        extreme_cells : list[np.ndarray]
            The cell locations containing extreme data, one list per holdout
            set.

        """
        extreme_cells: list[np.ndarray] = []
        for holdout in range(config.HOLDOUTS):
            large_data = self.original_data[holdout] > limits["upper"]
            small_data = self.original_data[holdout] < limits["lower"]
            extreme_data = large_data | small_data
            extreme_cells.append(np.argwhere(extreme_data))
        return extreme_cells

    @staticmethod
    def _get_selected_values(
        data: list[np.ndarray], cells: list[np.ndarray]
    ) -> list[np.ndarray]:
        """Return selected cells in each holdout set.

        This uses a method described at
        https://stackoverflow.com/questions/19821425
        """
        values: list[np.ndarray] = []
        for holdout in range(config.HOLDOUTS):
            cells_list = cells[holdout].T.tolist()
            values.append(data[holdout][tuple(cells_list)])
        return values

    @staticmethod
    def _cells_to_unique_rows(cells: list[np.ndarray]) -> list[np.ndarray]:
        all_rows: list[np.ndarray] = []
        for cell_array in cells:
            row_array = cell_array[:, 0]
            all_rows.append(np.unique(row_array))
        return all_rows


def get_quartiles(data: np.ndarray) -> dict[str, np.ndarray]:
    """Calculate the median, quartiles, IQR and limits for each variable.

    Parameters
    ----------
    original_data : np.ndarray
        An array of shape (num_items, num_variables).  The quartiles etc.
        will be calculated for each variable (column).

    Returns
    -------
    stats : dict[str, np.ndarray]
        A dictionary of arrays containing quartile-related data.
        The keys are: "median", "UQ", "LQ", "IQR", "upper", "lower", and
        each is an ndarray of shape (num_ariables,).
        The "upper" and "lower" limits are median +/- config.EXTREME_IQR * IQR

    """
    quartiles = np.percentile(data, [25, 50, 75], axis=0)
    stats = {
        "median": quartiles[1, :],
        "LQ": quartiles[0, :],
        "UQ": quartiles[2, :],
    }
    stats["IQR"] = stats["UQ"] - stats["LQ"]
    stats["upper"] = stats["median"] + config.EXTREME_IQR * stats["IQR"]
    stats["lower"] = stats["median"] - config.EXTREME_IQR * stats["IQR"]
    return stats


def iqr_normalise_data(
    data: np.ndarray, quartiles: dict[str, np.ndarray]
) -> np.ndarray:
    """Calculate the data normalised to multiples of IQR above median.

    Parameters
    ----------
    data : np.ndarray
        The original data array, shape (num_items, num_variables)
    quartiles : dict[str, np.ndarray]
        A dictionary containing keys "median" and "IQR", each an ndarray
        with shape (num_variables,).

    Returns
    -------
    normalised_data : np.ndarray
        The normalised data.

    """
    return (data - quartiles["median"]) / quartiles["IQR"]


def get_mean_sd(data: np.ndarray) -> dict[str, np.ndarray]:
    """Calculate the mean, s.d. and limits for each variable.

    Parameters
    ----------
    original_data : np.ndarray
        An array of shape (num_items, num_variables).  The mean etc.
        will be calculated for each variable (column).

    Returns
    -------
    stats : dict[str, np.ndarray]
        A dictionary of arrays containing mean and s.d.-related data.
        The keys are: "mean", "sd", "upper", "lower", and
        each is an ndarray of shape (num_ariables,).
        The "upper" and "lower" limits are mean +/- config.EXTREME_SD * sd

    """
    mean = np.mean(data, axis=0)
    sd = np.std(data, axis=0)
    stats = {"mean": mean, "sd": sd}
    stats["upper"] = stats["mean"] + config.EXTREME_SD * stats["sd"]
    stats["lower"] = stats["mean"] - config.EXTREME_SD * stats["sd"]
    return stats


def normalise_data(data: np.ndarray, mean_sd: dict[str, np.ndarray]) -> np.ndarray:
    """Calculate the data normalised to multiples of s.d. above mean.

    Parameters
    ----------
    data : np.ndarray
        The original data array, shape (num_items, num_variables)
    mean_sd : dict[str, np.ndarray]
        A dictionary containing keys "mean" and "sd", each an ndarray
        with shape (num_variables,) specifying the mean and s.d. to use.

    Returns
    -------
    normalised_data : np.ndarray
        The normalised data.

    """
    return (data - mean_sd["mean"]) / mean_sd["sd"]


def generate_all_params(show_progress: bool = True) -> Iterator[ParamDict]:
    """Generate all parameter combinations.

    Parameters
    ----------
    show_progress : bool
        Write a progress string to stdout when starting a new imputer,
        holdout set, or train/test missingness.

    Yields
    ------
    ParamDict
        Parameters (except for experiment).

    """
    params: ParamDict = {}

    for imputation in config.IMPUTERS:
        params["imputation"] = imputation
        if show_progress:
            print(f"Starting imputer {imputation}")

        for holdout in range(config.HOLDOUTS):
            params["holdout"] = holdout
            if show_progress:
                print(f"  Starting holdout {holdout}")

            for train_missing, test_missing in config.MISSINGNESS:
                params["train_missing"] = train_missing
                params["test_missing"] = test_missing
                if show_progress:
                    print(
                        f"    Starting missingness train {train_missing}, "
                        f"test {test_missing}"
                    )

                for validation in range(config.VALIDATIONS):
                    params["validation"] = validation

                    for repeat in range(config.REPEATS):
                        params["repeat"] = repeat

                        yield params.copy()


def generate_all_holdout_params(
    show_progress: bool = True,
) -> Iterator[ParamDict]:
    """Generate all holdout parameter dictionaries.

    This is probably only useful when iterating through the holdout sets.

    Parameters
    ----------
    show_progress : bool
        Write a progress string to stdout when starting a new holdout set.

    Yields
    ------
    ParamDict
        Parameter dict, only containing the holdout set.

    """
    params: ParamDict = {k: "" for k in PARAM_KEYS}

    for holdout in range(config.HOLDOUTS):
        params["holdout"] = holdout
        if show_progress:
            print(f"  Starting holdout {holdout}")

        yield params.copy()


def check_all_imputation_validities() -> bool:
    """Check that all the imputed data files match the original data files.

    Returns
    -------
    bool
        Whether all of the data matches.

    """
    all_match = True
    for experiment in config.EXPERIMENTS:
        for params in generate_all_params():
            bundle = DataBundle(experiment, [], params)
            bundle.sanitise_data()
            match = bundle.check_imputation_validity()
            if not match:
                print(f"** mismatch: {experiment} {params}")
                all_match = False

    return all_match


def find_all_missing_imputation_cells(experiment: str) -> list[np.ndarray]:
    """Identify all cells that are missing imputation data in some experiment.

    Parameters
    ----------
    experiment : str
        Either "mimic" or "synthetic".

    Returns
    -------
    missing : list[np.ndarray]
        Cells where at least one imputation has failed, with one list
        per holdout set.

    """
    # Collect up the lists of missing imputation data across all experiments
    missing_lists: list[list[np.ndarray]] = [[] for holdout in range(config.HOLDOUTS)]
    for params in generate_all_params():
        bundle = DataBundle(experiment, [], params)
        missing_data = bundle.locate_missing_imputed_data()
        if missing_data.size > 0:
            # need int(...) here to make mypy happy
            missing_lists[int(params["holdout"])].append(missing_data)

    # Now merge these lists to identify the unique cells missing imputation
    # data
    missing = []
    for holdout in range(config.HOLDOUTS):
        all_missing_cells = np.concatenate(missing_lists[holdout])
        unique_missing_cells = np.unique(all_missing_cells, axis=0)
        missing.append(unique_missing_cells)
    return missing
