#!/usr/bin/env python3

"""Handle random things within our framework.

This class ensures that experiments are repeatable, by centralising
all calls to random number generators.
"""

from typing import Optional

import numpy as np


class Random:
    """Initialise the generator with an optionally specified seed.

    Parameters
    ----------
    seed : int, optional
        The generator seed.

    """

    def __init__(self, seed: Optional[int] = None) -> None:
        self.rng = np.random.default_rng(seed)

    def random_direction(self, dim: int) -> np.ndarray:
        """Generate a unit vector in a random direction.

        Parameters
        ----------
        dim : int
            Dimension of vector to be generated.

        Returns
        -------
        unit_vector : np.ndarray
            A unit vector of shape (dim,).

        """
        vector = self.rng.normal(size=dim)
        vector_magnitude = np.linalg.norm(vector)
        unit_vector = vector / vector_magnitude
        return unit_vector

    def random_subspace(self, dim: int, subdim: int) -> np.ndarray:
        """Generate a set of `subdim` random orthonormal vectors.

        This effectively picks a random `subdim`-dimensional subspace
        of :math:`\\mathbb{R}^n`.

        Parameters
        ----------
        dim : int
            Dimension of vectors to be generated, or equivalently, the
            dimension of the ambient space.
        subdim : int
            The dimension of the vector subspace.

        Returns
        -------
        unit_vectors : np.ndarray
            An array of shape (dim, subdim); each unit_vectors[:, i] is
            a unit vector, and they are orthogonal to each other.

        Notes
        -----
        This algorithm assumes that any `subdim` random vectors will be
        linearly independent; it does not check that this is the case.

        """
        if subdim > dim:
            raise ValueError("subdim must be at most dim")
        rand_vectors = np.empty((dim, subdim), dtype=float)
        for i in range(subdim):
            rand_vectors[:, i] = self.rng.normal(size=dim)
        unit_vectors, _ = np.linalg.qr(rand_vectors, mode="reduced")
        return unit_vectors

    def random_split_idx(self, length: int) -> tuple[np.ndarray, np.ndarray]:
        """Randomly split the numbers from 0 to `length`-1 into two halves.

        Parameters
        ----------
        length : int
            Number of data items to split.

        Returns
        -------
        (indexes1, indexes2) : (np.ndarray, np.ndarray)
            A permutation of 0..`length`-1 in two equal (or almost equal)
            halves.

        """
        permuted_idx = self.rng.permutation(length)
        mid = length // 2
        return permuted_idx[:mid], permuted_idx[mid:]

    def random_split(self, data: np.ndarray) -> tuple[np.ndarray, np.ndarray]:
        """Randomly split an ndarray into two halves on the first dimension.

        Parameters
        ----------
        data : np.ndarray
            Data to split.

        Returns
        -------
        (data1, data2) : (np.ndarray, np.ndarray)
            A permutation of `data` in two equal (or almost equal) halves.

        """
        nrow = data.shape[0]
        idx1, idx2 = self.random_split_idx(nrow)
        return data[idx1, ...], data[idx2, ...]

    def bootstrap(
        self, data: np.ndarray, length: Optional[int] = None
    ) -> np.ndarray:
        """Bootstrap a sample from an ndarray on the first dimension.

        Parameters
        ----------
        data : np.ndarray
            Data to use for bootstrapping.
        length : Optional[int]
            The size of the bootstrap sample.  If not specified, defaults
            to the same size as the original data.

        Returns
        -------
        bootstrap_data : np.ndarray
            The bootstrapped data.

        """
        nrow = data.shape[0]
        if length is None:
            length = nrow
        bootstrap_idx = self.rng.choice(nrow, size=length, replace=True)
        return data[bootstrap_idx, ...]
