# Calculating imputation metrics using Wasserstein distances

This module provides tools for evaluating the "effectiveness" of an
imputation procedure.  We measure the distance of the imputed data
from the original data in d-dimensional space by taking random
directions or planes in the d-dimensional space and calculating the
Wasserstein distance between the imputed data and the original data in
this direction.

By repeating this calculation for a large number of random directions,
we are able to build a numerical description of the distance of the
imputed distribution from the original distribution.

This repository has scripts to calculate these metrics and to display
the results.  It requires the complete imputed datasets for either the
synthetic or MIMIC datasets produced by the `handling_missing_data`
experiments, and calculates Wasserstein distances between the imputed
data and original data in different ways.

This repository forms part of the supplementary material for the
paper:

Shadbahr, T. et al., "Classification of datasets with imputed missing
values: does imputation quality matter?"

## System requirements

This software should run on any operating system, though it has only
been tested on Linux and macOS systems.

The metric calculation code requires Python 3.9 or higher to be
installed; it has been tested with Python 3.9 and 3.10.

For plotting the results, R needs to be installed (this was tested on
R version 4.2.0), along with the following packages:

* OIdata
* dplyr
* ggplot2
* ggpubr
* ggsci
* pheatmap
* plyr
* readr
* scales
* stringr
* tidyverse
* grid
* viridis
* GGally
* RColorBrewer
* reshape2

## Installing the required Python packages

It is recommended to run this code in a virtual environment.  On a
UNIX-like system (macOS or Linux), run these commands in a suitable
terminal:

        python3 -m venv metrics-env

        source metrics-env/bin/activate

        pip install -r requirements.txt

The `imputation_metrics` package can either be installed using the
command:

        pip install .

or it can be used by setting `PYTHONPATH` explicitly, for example:

        PYTHONPATH=. scripts/experiment_wasserstein.py --help

to see the available command-line options.

Installation of this package typically takes under 1 minute.

## Using the general script

In order to calculate the Wasserstein distances between an original
dataset and an imputed datasets, as described in our paper, run the
command:

        scripts/calculate_wasserstein.py --output mydata original.csv imputed.csv

where `original.csv` and `imputed.csv` are CSV files with the
respective datasets; the output will be saved in various CSV files
called `mydata-*.csv`.  The script takes various options; use `--help`
to see these.

## Configuring the installation for the demo

The software needs to be told where the original and imputed data for
the MIMIC and SYNTHETIC experiments can be found.  Copy the file
`config.ini.example` to `config.ini` and edit the paths in the file to
point to the original and imputed data in the `handling_missing_data`
repository, and also specify a desired output directory for the
results.

## Demo

This software was designed to analyse the complete results of the
imputation experiments; there is no built-in demo mode (though see
below).  The complete synthetic imputation dataset takes about 24
hours to generate (see the `handling_missing_data` repository for
details on how to do this), though it can be done in less time by
computing various imputations in parallel or on multiple machines.

To calculate the Wasserstein distances for the synthetic dataset, use
the following command:

        scripts/experiment_wasserstein.py SYNTHETIC

(possibly with `PYTHONPATH=.` prepended, as discussed above); for the
MIMIC-III dataset, replace `SYNTHETIC` with `MIMIC`.

This takes about 5 minutes for the synthetic dataset and 15 minutes
for the larger MIMIC-III dataset on a good desktop machine, and will
deposit four CSV files in the specified output directory:

* `synthetic-directions.csv`: the random directions chosen.

* `synthetic-splits.json`: the random data splits used.

* `synthetic-baseline-distances.csv`: for each split and random
    direction, the Wasserstein distance between the two halves of the
    original complete data in that direction.

* `synthetic-distances.csv`: for each split and random direction, the
    Wasserstein distance between half of the original complete data
    and the other half of the imputed data in that direction.

(The file names will begin `mimic-` if the MIMIC-III imputations are
used instead.)

It is possible to modify the code to expect fewer imputations, and
this can be used for a demo.  To do this, edit the file
`imputation_metrics/config.py`, modifying the lines which specify
constants `IMPUTERS`, `MISSINGNESS`, `VALIDATIONS`, `HOLDOUTS` and
`REPEATS` to match what has been imputed.

## Analysing the results and producing plots

There are two directories with R code and Jupyter notebooks for
performing this analysis. The directory `results_analysis` comprises
three Jupyter notebooks (`Sliced_Wasserstein_cal.ipynb`,
`featurewise_dist_submission.ipynb`, and `STD_dist_cal.ipynb`) for
calculating the different discrepancy metrics addressed in the paper
Shadbahr, T. et al., "Classification of datasets with imputed missing
values: does imputation quality matter?".  Each of
`Sliced_Wasserstein_cal.ipynb` and `STD_dist_cal.ipynb` can be run in
about 10-20 minutes on a desktop, however for calculating the
feature-wise metrics, the run-time for
`featurewise_dist_submission.ipynb` for the MIMIC-III dataset is
approximately 2 hours and for the Simulated dataset is around 4 hours,
as it calculates all metrics for each marginal 3000 times.

After calculating all discrepancy metrics, the generated datasets (as
.csv files) are needed to pass to the files provided in plotting the
folder for further illustrations.  Additionally, the files
`Anova_final_analysis.R`, `prepro_classifers_res_after_imput.R`,
`bubble_plots_prepro.R` generate the files needed for running the
plotting codes for Anova and bubble plots respectively.  At the end of
running `Anova_final_analysis.R`, the results of the Anova analysis
are generated giving tables 2-5 in the supplementary material section.

The directory `plotting` contains the R code required for regenerating
the all the plots in this paper.  The list of the R scripts and their
corresponding plots are as follow:
* `Anova_analysis_polts.R` -> figures 4
* `bubble_plots_main_part.R` -> figure 3
* `bubble_plots_all_appendix.R` -> figures 15-17
* `check-outliers.ipynb` -> figure 8, 13
* `main_interpretability.ipynb` -> figure 14
* `metrics_heatmap_plots.R` -> figures 34, 10
* `MIMIC-FeatWise_ProDist_STDMetric_boxplot.R` -> figures 5-7,18,20-22,27-28
* `MIMIC_FeatWise_ProDist_STDmetric_vs_AUC_corr_plots.R` -> figure 9
* `SIM_FeatWise_ProDist_STDMetric_boxplot.R` -> figures 19, 23-26, 29-30
* `SIM_FeatWise_ProDist_STDmetric_vs_AUC_corr_plots.R` -> figures 31
* `wdists-visualise.R` -> figures 32-33


