#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Testing the Wasserstein distance code.
"""

import os
import json

import numpy as np
import pandas as pd

from imputation_metrics.metrics import WassersteinArgs
from imputation_metrics.normalisation import Normalisation
from imputation_metrics import metrics

testfiledir = os.path.dirname(__file__)


def loadcsv(filename: str) -> np.ndarray:
    data = np.loadtxt(os.path.join(testfiledir, filename), delimiter=",", skiprows=1)
    return data


def loadpd(filename: str) -> pd.DataFrame:
    data = pd.read_csv(os.path.join(testfiledir, filename))
    return data


def test_imputation_distances():
    wargs = WassersteinArgs(
        n_splits=4,
        n_directions=3,
        marginals=False,
        seed=20,
        normalise_components=Normalisation.NONE,
        normalise_distances=Normalisation.STANDARDISE,
    )

    original_data = loadcsv("original-data.csv")
    imputed_data = loadcsv("imputed-data.csv")

    imp_results = metrics.imputation_distances_from_data(
        original_data, imputed_data, wargs
    )

    with open(
        os.path.join(testfiledir, "test-distances-splits.json"), "r", encoding="ascii"
    ) as splitfile:
        expected_splits = json.load(splitfile)

    for splitnum, split in enumerate(imp_results.splits):
        for i in [0, 1]:
            assert expected_splits[str(splitnum)][i] == split[i].tolist()

    expected_directions = loadcsv("test-distances-directions.csv")
    for i, dirn in enumerate(imp_results.directions):
        assert np.allclose(dirn, expected_directions[i, 1:])

    expected_baseline_distances = loadpd("test-distances-baseline-distances.csv")
    assert imp_results.baseline_distances.drop(columns=["distance"]).equals(
        expected_baseline_distances.drop(columns=["distance"])
    )
    assert np.allclose(
        imp_results.baseline_distances["distance"].to_numpy(),
        expected_baseline_distances["distance"].to_numpy(),
    )

    expected_distances = loadpd("test-distances-distances.csv")
    dist_cols = [
        "distance",
        "baseline_distance",
        "distance_ratio",
        "log_distance_ratio",
    ]
    assert imp_results.distances.drop(columns=dist_cols).equals(
        expected_distances.drop(columns=dist_cols)
    )
    assert np.allclose(
        imp_results.distances[dist_cols].to_numpy(),
        expected_distances[dist_cols].to_numpy(),
    )
