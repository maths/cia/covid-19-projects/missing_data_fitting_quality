#!/usr/bin/env python3

import numpy as np
import pytest
from pytest import approx
from hypothesis import given, strategies

from imputation_metrics import randoms


MAXDIM = 100


@pytest.fixture(scope="module")
def imp_random() -> randoms.Random:
    return randoms.Random(seed=42)


@given(dim=strategies.integers(1, MAXDIM))
def test_random_direction(imp_random, dim):
    unit_vector = imp_random.random_direction(dim)
    unit_vector_length = np.linalg.norm(unit_vector)
    assert unit_vector_length == approx(1.0)
    assert unit_vector.shape == (dim, )


@strategies.composite
def dim_subdim(draw):
    dim = draw(strategies.integers(2, MAXDIM))
    subdim = draw(strategies.integers(1, dim))
    return (dim, subdim)


@given(dim_subdim=dim_subdim())
def test_random_subspace(imp_random, dim_subdim):
    dim, subdim = dim_subdim
    subspace = imp_random.random_subspace(dim, subdim)
    # check dimension
    assert subspace.shape == (dim, subdim)
    for subd in range(subdim):
        unit_vector = subspace[:, subd]
        # check length
        assert np.linalg.norm(unit_vector) == approx(1.0)
        # check orthogonality
        for subd2 in range(subd):
            unit_vector2 = subspace[:, subd2]
            assert np.dot(unit_vector, unit_vector2) == approx(0.0)
